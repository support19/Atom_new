class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|

    	t.belongs_to :customer, index:true
    	t.belongs_to :cust_address
    	t.belongs_to :executive
    	t.integer :pslot_id 
    	t.integer :dslot_id
      t.date  :pickupdate
      t.date :deliverydate
    	t.integer  :status
    	t.integer :mobile_alt
    	t.integer :exp_clothes

      t.timestamps null: false
    end
  end
end
