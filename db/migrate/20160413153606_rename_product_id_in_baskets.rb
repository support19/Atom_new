class RenameProductIdInBaskets < ActiveRecord::Migration
  def change
    rename_column :baskets, :product_id, :service_id
  end
end
