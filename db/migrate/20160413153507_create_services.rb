class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :cloth
      t.integer :type
      t.boolean :per_kg
      t.boolean :per_peice
      t.integer :price
      t.boolean :men
      t.boolean :women

      t.timestamps null: false
    end
  end
end
