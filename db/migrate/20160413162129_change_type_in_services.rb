class ChangeTypeInServices < ActiveRecord::Migration
  def change
    remove_column :services, :type
    add_column :services, :service_type, :integer
  end
end
