class AddEmailToExecutives < ActiveRecord::Migration
  def change
    add_column :executives, :email, :string
    remove_column :executives, :password_digest
    add_column :executives, :encrypted_password,:string
    add_column :executives, :remember_created_at, :datetime
  end
end
