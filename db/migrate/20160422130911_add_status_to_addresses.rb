class AddStatusToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :status, :boolean
  end
end
