class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
	t.belongs_to :customer, index:true
	t.string :code
	t.integer :credits
	t.boolean :active
    t.timestamps null: false
    end
  end
end
