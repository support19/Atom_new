class AddOtpToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :otp, :integer
    add_column :customers, :mobile_verified, :boolean
  end
end
