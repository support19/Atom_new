class AddChatIdToExecutives < ActiveRecord::Migration
  def change
    add_column :executives, :chat_id, :string
  end
end
