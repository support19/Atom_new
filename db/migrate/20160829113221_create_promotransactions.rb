class CreatePromotransactions < ActiveRecord::Migration
  def change
    create_table :promotransactions do |t|
      t.belongs_to :promotion , index:true	
      t.belongs_to :customer  , index:true
      t.belongs_to   :schedule_id
      t.integer :orderflag
      t.timestamps null: false
    end
  end
end
