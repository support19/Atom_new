class AddOncallFlagToCustomers < ActiveRecord::Migration
  def change
  	add_column :customers, :oncall, :boolean
  end
end
