class ChangePhoneNumberToStringInVendors < ActiveRecord::Migration
  def change
    remove_column :vendors, :phone1
    remove_column :vendors, :phone2
    add_column :vendors, :phone1, :string
    add_column :vendors, :phone2, :string
  end
end
