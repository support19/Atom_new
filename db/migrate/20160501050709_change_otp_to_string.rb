class ChangeOtpToString < ActiveRecord::Migration
  def change
    remove_column :customers, :otp, :integer
    add_column :customers, :otp, :string
  end
end
