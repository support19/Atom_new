class RemoveKilosFromBasket < ActiveRecord::Migration
  def change
    remove_column :baskets, :kilos
    add_column :orders, :kilos, :decimal
  end
end
