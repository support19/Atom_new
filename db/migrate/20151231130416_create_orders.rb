class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|

    	t.belongs_to :customer, index:true
    	t.string :schedule_id
    	t.date :delivery_date
        t.integer :dslot_id
    	t.integer :picked_by
    	t.integer :delivered_by
    	t.integer :vendor_id
    	t.string :status
    	t.datetime :vendor_picked_time
    	t.datetime :vendor_delivered_time
    	t.string :amount
    	t.string :payment_id
    	t.string :payment_status
    	t.string :payment_type
        t.datetime :actual_delivery_time
        t.string :notes



      t.timestamps null: false
    end
  end
end
