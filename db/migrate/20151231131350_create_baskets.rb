class CreateBaskets < ActiveRecord::Migration
  def change
    create_table :baskets do |t|

    	t.belongs_to :order
    	t.belongs_to :product
    	t.integer  :quantity

      t.timestamps null: false
    end
  end
end
