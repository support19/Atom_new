class ChangeMobileTypeInExecutives < ActiveRecord::Migration
  def change
    remove_column :executives, :mobile
    add_column :executives, :mobile, :string
  end
end
