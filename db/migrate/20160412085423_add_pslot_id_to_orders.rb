class AddPslotIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :pslot_id, :integer
    add_column :orders, :address_id, :integer
  end
end
