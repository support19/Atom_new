class AddActiveFlagToBagNumber < ActiveRecord::Migration
  def change
	add_column :bags, :active, :boolean
  end
end
