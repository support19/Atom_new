class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|

      t.belongs_to :referral
      t.belongs_to :customer
      t.integer :orderflag
      t.timestamps null: false
    end
  end
end
