class AddExecutiveIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :executive_id, :integer
  end
end
