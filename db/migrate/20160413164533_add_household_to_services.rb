class AddHouseholdToServices < ActiveRecord::Migration
  def change
    add_column :services, :household, :boolean
  end
end
