class ChangeDefaultPaymentStatus < ActiveRecord::Migration
  def change
  change_column :orders, :payment_status, :string ,  :default => "cod"

  end
end
