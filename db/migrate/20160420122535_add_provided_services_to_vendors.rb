class AddProvidedServicesToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :provided_services, :string, array: true
  end
end
