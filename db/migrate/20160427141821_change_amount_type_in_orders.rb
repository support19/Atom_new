class ChangeAmountTypeInOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :amount, :string
    add_column :orders, :amount, :decimal
  end
end
