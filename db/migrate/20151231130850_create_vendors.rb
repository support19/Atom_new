class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|

    	t.string :name
    	t.integer :phone1
    	t.integer :phone2
    	t.string :street
    	t.string :address
        t.string :area
        t.string :lat
        t.string :lng

      t.timestamps null: false
    end
  end
end
