class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|

     t.belongs_to :customer, index:true
     t.string :lat
     t.string :lng
     t.string :flat
     t.string :street
     t.string :landmark
     t.string :city
     t.string :state
     t.integer :pincode
     t.string :directions
     t.string :tag


      t.timestamps null: false
    end
  end
end
