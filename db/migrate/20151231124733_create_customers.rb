class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :provider
      t.string :fb_id
    	t.string :fb_name
        t.string :fb_email
        t.string :goo_id
        t.string :goo_name
        t.string :goo_email
        t.string :mobile
        t.string :session_token
        t.string :oauth_token
        t.datetime :oauth_expires_at
        t.string :device_id
        t.string :gcm_registration_id
        t.boolean :active
        t.string :app_version
        t.string :updated_by
      t.timestamps null: false
    end
  end
end
