class AddKilosToBaskets < ActiveRecord::Migration
  def change
    add_column :baskets, :kilos, :integer
  end
end
