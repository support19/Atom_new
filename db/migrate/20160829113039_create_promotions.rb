class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :code
      t.datetime :valid_from
      t.datetime  :valid_to
      t.boolean :creditflag
      t.integer :value
      t.boolean :active
      t.timestamps null: false
    end
  end
end
