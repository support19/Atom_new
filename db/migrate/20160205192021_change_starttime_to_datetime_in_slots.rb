class ChangeStarttimeToDatetimeInSlots < ActiveRecord::Migration
  def change
  	remove_column :slots, :starttime
  	remove_column :slots, :endtime
  	add_column :slots, :starttime, :datetime
  	add_column :slots, :endtime, :datetime
  end
end
