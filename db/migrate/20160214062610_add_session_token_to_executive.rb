class AddSessionTokenToExecutive < ActiveRecord::Migration
  def change
  	add_column :executives, :session_token, :string
  end
end
