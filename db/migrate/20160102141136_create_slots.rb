class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
    	t.time :starttime
    	t.time :endtime
      t.timestamps null: false
    end
  end
end
