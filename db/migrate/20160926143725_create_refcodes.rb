class CreateRefcodes < ActiveRecord::Migration
  def change
    create_table :refcodes do |t|
      t.string :refcode
      t.timestamps null: false
    end
  end
end
