class AddUniqueKeyToBagnumber < ActiveRecord::Migration
  def change
	add_index :bags, :number, :unique => true
  end
end
