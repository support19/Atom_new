class RenamePasswordToPasswordDigest < ActiveRecord::Migration
  def change
  	rename_column :executives, :password, :password_digest
  end
end
