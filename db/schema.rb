# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170127120850) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "lat"
    t.string   "lng"
    t.string   "flat"
    t.string   "street"
    t.string   "landmark"
    t.string   "city"
    t.string   "state"
    t.integer  "pincode"
    t.string   "directions"
    t.string   "tag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "status"
  end

  add_index "addresses", ["customer_id"], name: "index_addresses_on_customer_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "bags", force: :cascade do |t|
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "active"
  end

  add_index "bags", ["number"], name: "index_bags_on_number", unique: true, using: :btree

  create_table "baskets", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "service_id"
    t.integer  "quantity"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.boolean  "piece_model"
    t.decimal  "quantity_weight"
    t.string   "bag_number"
  end

  create_table "customers", force: :cascade do |t|
    t.string   "provider"
    t.string   "fb_id"
    t.string   "fb_name"
    t.string   "fb_email"
    t.string   "goo_id"
    t.string   "goo_name"
    t.string   "goo_email"
    t.string   "mobile"
    t.string   "session_token"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "device_id"
    t.string   "gcm_registration_id"
    t.boolean  "active"
    t.string   "app_version"
    t.string   "updated_by"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "mobile_verified"
    t.string   "otp"
    t.boolean  "oncall"
  end

  create_table "executives", force: :cascade do |t|
    t.string   "name"
    t.boolean  "loggedin"
    t.string   "photoloc"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "status"
    t.string   "session_token"
    t.string   "chat_id"
    t.string   "mobile"
    t.string   "email"
    t.string   "encrypted_password"
    t.datetime "remember_created_at"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "schedule_id"
    t.date     "delivery_date"
    t.integer  "dslot_id"
    t.integer  "picked_by"
    t.integer  "delivered_by"
    t.integer  "vendor_id"
    t.datetime "vendor_picked_time"
    t.datetime "vendor_delivered_time"
    t.string   "payment_id"
    t.string   "payment_status",        default: "cod"
    t.string   "payment_type"
    t.datetime "actual_delivery_time"
    t.string   "notes"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "executive_id"
    t.integer  "pslot_id"
    t.integer  "address_id"
    t.integer  "status"
    t.integer  "type"
    t.decimal  "kilos"
    t.integer  "discount",              default: 0
    t.decimal  "amount"
    t.integer  "quantity"
  end

  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "level1"
    t.string   "level2"
    t.string   "level3"
    t.string   "level4"
    t.string   "level5"
    t.decimal  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string   "code"
    t.datetime "valid_from"
    t.datetime "valid_to"
    t.boolean  "creditflag"
    t.integer  "value"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotransactions", force: :cascade do |t|
    t.integer  "promotion_id"
    t.integer  "customer_id"
    t.integer  "schedule_id_id"
    t.integer  "orderflag"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "promotransactions", ["customer_id"], name: "index_promotransactions_on_customer_id", using: :btree
  add_index "promotransactions", ["promotion_id"], name: "index_promotransactions_on_promotion_id", using: :btree

  create_table "refcodes", force: :cascade do |t|
    t.string   "refcode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referrals", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "code"
    t.integer  "credits"
    t.boolean  "active"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "referrals", ["customer_id"], name: "index_referrals_on_customer_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "cust_address_id"
    t.integer  "executive_id"
    t.integer  "pslot_id"
    t.integer  "dslot_id"
    t.date     "pickupdate"
    t.date     "deliverydate"
    t.integer  "status"
    t.integer  "mobile_alt"
    t.integer  "exp_clothes"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "schedules", ["customer_id"], name: "index_schedules_on_customer_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "cloth"
    t.boolean  "per_kg"
    t.boolean  "per_peice"
    t.integer  "price"
    t.boolean  "men"
    t.boolean  "women"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "service_type"
    t.boolean  "household"
  end

  create_table "slots", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "starttime"
    t.datetime "endtime"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "referral_id"
    t.integer  "customer_id"
    t.integer  "orderflag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end


  create_table "vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "street"
    t.string   "address"
    t.string   "area"
    t.string   "lat"
    t.string   "lng"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "provided_services",              array: true
    t.string   "phone1"
    t.string   "phone2"
    t.string   "email"
  end

end
