# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
(0..1000).each do |num|
  Slot.create(starttime: (("2016-05-03 07:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 09:00:00").in_time_zone(Time.zone) + num.days ))
  Slot.create(starttime: (("2016-05-03 09:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 11:00:00").in_time_zone(Time.zone) + num.days ))
  Slot.create(starttime: (("2016-05-03 11:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 13:00:00").in_time_zone(Time.zone) + num.days ))
  Slot.create(starttime: (("2016-05-03 14:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 16:00:00").in_time_zone(Time.zone) + num.days )) if ("2016-05-03".to_date + num.days).saturday? || ("2016-05-03".to_date + num.days).sunday?
  Slot.create(starttime: (("2016-05-03 16:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 18:00:00").in_time_zone(Time.zone) + num.days ))
  Slot.create(starttime: (("2016-05-03 18:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 20:00:00").in_time_zone(Time.zone) + num.days ))
  Slot.create(starttime: (("2016-05-03 20:00:00").in_time_zone(Time.zone) + num.days), endtime: (("2016-05-03 22:00:00").in_time_zone(Time.zone) + num.days ))
end

(0..2).each do|c|
Customer.create(goo_name:"abcd"+c.to_s,mobile:"9440013"+c.to_s,goo_email: "vinilapple@gmail.com")
end
