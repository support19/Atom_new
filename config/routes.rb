Rails.application.routes.draw do



 	match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
	match 'auth/failure', to: redirect('/'), via: [:get, :post]
	match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  resources :login do
  end

  resources :order, except: :show do

  end

  resources :address, except: :show do
  end

  resources :offer, except: :show do
  end

  get "address/slots" => "address#slots"
  post "login/index" => "login#index"
  get "order/index" => "order#index"
  root 'login#index'
end
