# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->

 pickup = ->

    pickupdate = $('#pickupdate :selected').text()
    $('#pslot').empty()
    $.ajax '/address/slots',
      type: 'GET'
      dataType: 'json'
      data: 'date': pickupdate
      success: (data) ->
        $.each data.slots, (key, value) ->
          $('#pslot').append '<option>' + value.starttime + ' - ' + value.endtime + '</option>'
          return
        return
    return

    return

  $('#pickupdate').change pickup
  return
