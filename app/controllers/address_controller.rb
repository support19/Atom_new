class AddressController < ApplicationController


  def create
    # session[:location] = params[:location]
    @pickupdate = Date.parse(params[:pickupdate]).strftime("%d/%m/%Y").to_date.to_s
    @pslot = params[:pslot]
    @pstarttime = @pickupdate  + ' ' + @pslot

    @pid = Slot.find_by_starttime(@pstarttime.in_time_zone).id

    if params[:fb_id]
      @customer = Customer.find_by_fb_id (params[:fb_id])

    else
      @customer = Customer.find_by_goo_id (params[:goo_id])
    end

    @customer.update(mobile: params[:mobile])
    @adress = @customer.addresses.create(:flat=>params[:flatnum],:street =>params[:street],:landmark =>params[:landmark],:pincode => params[:pincode])
    @schedule = @customer.schedules.create(:pslot_id=>@pid,:cust_address_id=>@adress.id)
    @customer.orders.create(:pslot_id=>@pid,:schedule_id=>@schedule.id)

    #OrderMailer.schedule_email(@customer,@schedule,@adress,@pickupdate,@pslot,@deldate,@dslot).deliver_later

    flash[:success] = "Order Placed"
    redirect_to '/order/index'

  end

  def index
  end

  def slots
		@slots = Slot.where(starttime: ((Date.parse(params[:date]).strftime("%d/%m/%Y").to_date)..((Date.parse(params[:date]).strftime("%d/%m/%Y").to_date) + 1.day))).where("starttime > ?", Time.current)
    params.permit(:date)
	  respond_to do |format|
      format.json  { render json: { slots: prettify_slots }, status: :ok }

    end
end


  private

  def prettify_slots
    @pretty_slots = []
    @slots.each do |slot|
      @pretty_slots.push(id: slot.id, starttime: slot.starttime.strftime('%I %p'), endtime: slot.endtime.strftime('%I %p'))
    end
    @pretty_slots
  end

end
