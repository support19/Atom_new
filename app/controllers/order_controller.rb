class OrderController < ApplicationController

  def index
    @orders = Order.includes(:customer).where.not(status: [ 4, 5 ])
  end

end
