class SessionsController < ApplicationController
  def create
    customer = Customer.from_omniauth(env['omniauth.auth'])
    session[:customer_id] = customer.id
    redirect_to '/address'
  end

  def destroy
    session[:customer_id] = nil
    redirect_to root_url
  end
end
