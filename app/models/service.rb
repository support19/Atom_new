class Service < ActiveRecord::Base
  enum service_type: [:standard_only_ironing, :standard_wash_and_fold, :standard_wash_and_iron, :premium, :dry_cleaning]
  has_one :basket

  def kilo_model?
    per_kg
  end

  def piece_model?
    per_peice
  end
end
