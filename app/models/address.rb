class Address < ActiveRecord::Base
  belongs_to :customer
  has_many :schedules
  belongs_to :order

  before_create :set_status

  def full_address
    "#{flat}, #{street}, #{landmark}, #{city}"
  end

  private

  def set_status
    self.status = true
  end
end
