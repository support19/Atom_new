class Schedule < ActiveRecord::Base
  belongs_to :customer
  belongs_to :executive
  belongs_to :address, foreign_key: 'cust_address_id'
  enum status: [:active, :completed, :cancelled]
  has_many :orders
  before_create :set_status

  def alloted?
    executive_id.present?
  end

  def full_address
    address.full_addresss
  end

  def pick_up_time
    Slot.find(pslot_id).starttime.strftime('%d/%m/%y %I:%M')
  end

  def delivery_time
    Slot.find(dslot_id).starttime.strftime('%d/%m/%y %I:%M')
  end

  def duration
    Slot.find(pslot_id).duration
  end

  def pick_up_date
    Slot.find(pslot_id).date
  end

  def sms_content(customer)
    "Dear #{customer.name} " + Figaro.env.MSG_SCHEDULE_MESSAGE1 + id.to_s + ' ' + Figaro.env.MSG_SCHEDULE_MESSAGE2 + duration + ' on ' + pick_up_date.to_s
  end

  private

  def set_status
    self.status = 'active'
  end
end
