class Slot < ActiveRecord::Base
## Schema Information
## t.datetime     "starttime"
## t.datetime     "endtime"
## t.datetime "created_at", null: false
## t.datetime "updated_at", null: false
  has_many :orders

  def self.next_five_days
  	#TODO: show only available slots
    slots = Slot.where(starttime: (Time.current)..((Time.current) + 5.days))
  end

  def self.pick_up_dates
  	pick_up_dates = []
  	(0..4).each do |i|
  		pick_up_dates.push((Time.current.to_date + i.days).strftime('%a, %b %d %y'))
  	end
  	pick_up_dates
  end

  def self.delivery_dates_oncall
    d_dates = []
    (2..11).each do |i|
        d_dates.push((Time.current.to_date + i.days).strftime('%a, %b %d %y'))
      end
      d_dates
  end
  # def self.delivery_dates(stype)
  #
  # 	(k..11).each do |i|
  # 		delivery_dates.push((Time.current.to_date + i.days).strftime('%a, %b %d'))
  # 	end
  # 	delivery_dates
  # end

  def time
    starttime.strftime("%H:%M")
  end

  def duration
    "#{starttime.strftime('%I %p')} - #{endtime.strftime('%I %p')}"
  end

  def date
    starttime.to_date
  end

end
