class Order < ActiveRecord::Base
  belongs_to :customer
  belongs_to :schedule
  belongs_to :executive
  belongs_to :pickup_slot, class_name: 'Slot', foreign_key: 'pslot_id'
  belongs_to :delivery_slot, class_name: 'Slot', foreign_key: 'dslot_id'
  has_one :address, through: :schedule
  has_many :baskets
  before_create :set_status

  enum status: [:scheduled, :pickup, :wash, :delivery, :completed, :cancelled]
  enum type: [:standard_only_ironing, :standard_wash_and_fold, :standard_wash_and_iron, :premium, :dry_cleaning]

  self.inheritance_column = nil

  def pick_up?
    status == 'scheduled' || status == 'pickup'
  end

  def quantity
    baskets.map(&:quantity).sum
  end

  def delivery?
    status == 'delivery'
  end

  def picked_up?
    status == 'pickup'
  end

  def alloted?
    executive_id.present?
  end

  def only_one?
    schedule.orders.count == 1
  end

  def pickup_date
    pickup_slot.date
  end

  def pickup_time
    pickup_slot.starttime.strftime('%-l:%M %p')
  end

  def delivery_date
    delivery_slot.date
  end

  def delivery_time
    delivery_slot.starttime.strftime('%-l:%M %p')
  end

  def full_address
    address.full_address
  end

  def pickup_slot_duration
    pickup_slot.duration
  end

  def delivery_slot_duration
    delivery_slot.duration
  end

  def bill
    piece_baskets = Order.baskets_and_services(id).try(:baskets); piece_baskets ||= []
    piece_sum = piece_baskets.inject(0) { |sum, b| sum + b.cost }
    weight_baskets = Order.baskets_and_services_kg(id).try(:baskets); weight_baskets ||= []
    weight_sum = weight_baskets.inject(0) { |wsum, wb| wsum.to_f + wb.weightcost }

    # service = Service.where(cloth: "TROUSER", service_type: Service.service_types[self.type]).first
    # sum = sum + (self.kilos.to_f)*(service.price) unless self.kilos.nil?

    sum = piece_sum + weight_sum
    sum
  end

  def self.baskets_and_services(id)
    Order.includes(baskets: :service).where('baskets.piece_model = ? and orders.id = ?', true, id).references(:baskets).first
  end

  def self.baskets_and_services_kg(id)
    Order.includes(baskets: :service).where('baskets.piece_model = ? and orders.id = ?', false, id).references(:baskets).first
  end

  def save_payment
    if customer.first_order? self && bill >= 200
      update(amount: bill, discount: 100)
    else
      update(amount: bill)
    end
  end

  def pickup_sms
    "Your pickup for #{id} was successful. Order Details: Weight: #{kilos.to_f} kilos "\
    "and total number of garments are #{quantity}"
  end

  private

  def set_status
    self.status = 'scheduled'
  end
end
