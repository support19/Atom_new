class Basket < ActiveRecord::Base
  belongs_to :order
  belongs_to :service
  belongs_to :user

  def cost
    quantity * service.price
  end

  def weightcost
    quantity_weight.to_f * service.price
  end
end
