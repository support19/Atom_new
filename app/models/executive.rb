require 'securerandom'
class Executive < ActiveRecord::Base
  devise :database_authenticatable,
         :rememberable, :validatable
  has_many :schedules
  has_many :orders
  has_many :baskets, through: :orders

  before_create :generate_session_token

  def active?
    status
  end

  def logged_in?
    loggedin
  end

  def send_telegram(order)
    url = 'https://api.telegram.org/bot206204441:AAH5TjnP6JVAoiZ6WQ4NG6LnHAvwhrs4TvY/sendMessage?'
    url += "chat_id=#{chat_id}&text=name: #{order.customer.name}\n mobile: #{order.customer.mobile}\n address:
								#{order.full_address}\n expected_clothes: #{order.schedule.exp_clothes}\n
								pick_up_time: #{order.pickup_time}\ndelivery_time: #{order.delivery_time}"
    url += "\nPlease collect #{order.amount - order.discount} rupees" if order.delivery?
    HTTParty.get(url)
  end

  private

  def generate_session_token
    loop do
      self.session_token = SecureRandom.base64.tr('+/=', 'Qrt')
      break session_token unless Executive.exists?(session_token: session_token)
    end
  end
end
