require "securerandom"
class Customer < ActiveRecord::Base
  has_many :addresses
  has_many :schedules
	has_many :orders
	before_create :generate_session_token

  def self.from_omniauth(auth)
    where(provider: auth.provider).first_or_create do |customer|
      if (auth.provider == 'facebook')
        customer.fb_id = auth.uid
        customer.fb_name = auth.info.name
        customer.fb_email = auth.info.email

      else

        customer.goo_id = auth.uid
        customer.goo_name = auth.info.name
        customer.goo_email = auth.info.email

      end

      customer.oauth_token = auth.credentials.token
      customer.oauth_expires_at = Time.at(auth.credentials.expires_at)
      customer.save!
    end
  end

  def email
  email= goo_email||fb_email
  #has_many :orders
  #before_create :generate_session_token
  end

  def name
    name = fb_name || goo_name
  end

  def send_otp(mobile)
    generate_otp(mobile)
    HTTParty.get(msg91_url(mobile, Figaro.env.MSG_OTP_MESSAGE.chomp + self.otp.to_s + " We hope to serve you soon"))
  end

  def generate_otp(mobile)
    otp = (4.times.map{rand(10)}.join).to_s.rjust(4, '0')
    update(otp: otp, mobile: mobile, mobile_verified: false)
    otp
  end

  def msg91_url(mobile, message)
    "https://control.msg91.com/api/sendhttp.php?authkey=#{Figaro.env.MSG_AUTH_KEY.chomp}"\
    "&mobiles=#{mobile}&message=#{message}"\
    "&sender=#{Figaro.env.MSG_SENDER_ID}&route=4"
  end

  def first_order? order
    orders.order("created_at DESC").try(:first) == order
  end

	private
	def generate_session_token
		loop do
      self.session_token = SecureRandom.base64.tr('+/=', 'Qrt')
      break self.session_token unless Customer.exists?(session_token: self.session_token)
    end
	end

end
